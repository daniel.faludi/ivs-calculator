"""This module contains basic mathematical functions."""

import math

class MatlibError(Exception):
    """Base exception class for matlib."""
    def __init__(self, message):
        """Parameters
        ----------
        message : str
            Message to be displayed."""
        self.message = message


class FactorialError(MatlibError):
    """Factorial exception raised when calculating factorial for a number less than zero."""
    pass


class DivisionByZero(MatlibError):
    """Raised when attempting to divide by zero."""
    pass


class ExponentialError(MatlibError):
    """Raised when the exp() function encouters a data type mismatch."""
    pass


class RootError(MatlibError):
    """Raised when the root() function encouters a data type mismatch or value error."""
    pass


def add(num1, num2):
    """Return num1 + num2.
    
    Parameters
    ----------
    num1 : float or int
        First number of the operation.
    num2 : float or int
        Second number of the operation.
            
    Returns
    -------
    result : float or int
        Result of addition."""
    return num1 + num2


def sub(num1, num2):
    """Return num1 - num2.
    
    Parameters
    ----------
    num1 : float or int
        First number of the operation.
    num2 : float or int
        Second number of the operation.
            
    Returns
    -------
    result : float or int
        Result of subtraction."""
    return num1 - num2


def mult(num1, num2):
    """Return num1 * num2.
    
    Parameters
    ----------
    num1 : float or int
        First number of the operation.
    num2 : float or int
        Second number of the operation.
            
    Returns
    -------
    result : float or int
        Result of multiplication."""
    return num1 * num2


def div(num1, num2):
    """Return num1 / num2, raise exception if num2 is zero.
    
    Parameters
    ----------
    num1 : float or int
        First number of the operation.
    num2 : float or int
        Second number of the operation.
            
    Returns
    -------
    result : float or int
        Result of division.
            
    Raises
    ------
    DivisionByZero
        Raised when num2 is equal to zero."""
    if num2 == 0:
        raise DivisionByZero('Attempted to divide by zero.')

    return num1 / num2


def fact(num):
    """Return the factorial of num. If num is not an integer, it will be rounded towards zero.
    
    Parameters
    ----------
    num : int
        The number of which the factiorial should be returned. Must be greater or equal to zero.

    Returns
    -------
    factorial : int
        The factorial of num.

    Raises
    ------
    FactorialError
        Raised when num is less than zero.
    """
    num = int(num)

    if num < 0:
        raise FactorialError('Attempted to calculate factorial of a number less than zero.')

    factorial = 1
    for i in range(1, num+1):
        factorial = factorial * i

    return factorial


def exp(x, e):
    """Return an exponential of x to the power of e.
    
    Parameters
    ----------
    x : float or int
        The base of the exponent. Must be a floating point number or an integer.
    e : int
        The exponent. Must be an integer.
    
    Returns
    -------
    result : float or int
        The result of x**e.

    Raises
    ------
    ExponentialError
        Raised when base and exponent data types are incorrect.
    """
    if (not isinstance(x, int)) and (not isinstance(x, float)):
        raise ExponentialError('The base of an exponent was not float or int.')

    if not isinstance(e, int):
        raise ExponentialError('The exponent is not an integer.')

    return x**e


def root(n, x):
    """Return the n-th root of x according to the formula x**(1/n).
    
    Parameters
    ----------
    x : float or int
        The base of the root. Must be a floating point number or an integer.
    n : int
        The n-th root. Must be a positive integer (zero excluded).
    
    Returns
    -------
    result : float or int
        The result of x**(1/n).

    Raises
    ------
    RootError
        Raised when parameter data types are incorrect or when n is negative or zero.
    """
    if (not isinstance(x, int)) and (not isinstance(x, float)):
        raise RootError('Base of the root is not float or int.')

    if (not isinstance(n, int)):
        raise RootError('The n-th root is not an int.')

    if n <= 0:
        raise RootError('The n-th root is negative or zero.')

    return x**(1/n)


def sine(num):
    """Return sine of num.

    Parameters
    ----------
    num : float
        The number of which the sine should be calculated.

    Returns
    -------
    sine : float
        The sine of num.
    """
    return math.sin(num)


def solve_equation(calc, textfield_as_list):
    """
    Performs infix to postfix conversion of the expression, solves it and saves the result to label.

    Parameters
    ----------
    calc : CalcGUI
        Object of class calcGUI.
    textfield_as_list : list
        Expression as list.
    """
    flag = 0
    for c in textfield_as_list:
        if c.isnumeric() or is_float(c):
            flag = 1
    
    if flag == 0:
        return

    if textfield_as_list[-1] in {'+', '-', '*', '/', '(', '^', 'sin(', '\u221A'}:
        return
    
    if calc.left_paren_count != 0:
        return

    postfix = []
    stack = []

    expression = textfield_as_list

    for c in expression:
        if c.isnumeric() or is_float(c):
            postfix.append(c)
        elif c == '(':
            stack.append(c)
        elif c in {'+', '-'}:
            if not stack or stack[-1] in {'sin(', '(', ')'}:
                stack.append(c)
            else:
                for s in reversed(stack):
                    if s in {'sin(', '(', ')'}:
                        break
                    postfix.append(stack.pop())
                stack.append(c)
        elif c in {'*', '/', '!'}:
            if not stack or stack[-1] in {'+', '-', 'sin(', '(', ')'}:
                stack.append(c)
            else:
                for s in reversed(stack):
                    if s in {'+', '-', 'sin(', '(', ')'}:
                        break
                    postfix.append(stack.pop())
                stack.append(c)
        elif c in {'^', '\u221A'}:
            if not stack or stack[-1] in {'+', '-', '*', '/', 'sin(', '(', ')'}:
                stack.append(c)
            else:
                for s in reversed(stack):
                    if s in {'+', '-', '*', '/', 'sin(', '(', ')'}:
                        break
                    postfix.append(stack.pop())
                stack.append(c)
        elif c == 'sin(':
            if not stack or stack[-1] in {'+', '-', '*', '/', 'sin(', '('}:
                stack.append(c)
            else:
                for s in reversed(stack):
                    if s in {'+', '-', 'sin(', '('}:
                        break
                    postfix.append(stack.pop())
                stack.append(c)
        elif c == ')':
            for s in reversed(stack):
                if s == '(':
                    stack.pop()
                    break
                if s == 'sin(':
                    postfix.append(stack.pop())
                    break
                postfix.append(stack.pop())

    for _ in range(len(stack)):
        postfix.append(stack.pop())

    operations = {
        '+' : add, '-' : sub, '*' : mult, '/' : div, '^' : exp, 
        '\u221A' : root, '!' : fact, 'sin(' : sine
    }

    number_stack = []
    calc_flag = 0
    for c in postfix:
        if c.isnumeric():
            number_stack.append(int(c))
        elif is_float(c):
            number_stack.append(float(c))
        else:
            if c in {'+', '-', '*', '/', '^', '\u221A'}:
                op2, op1 = number_stack.pop(), number_stack.pop()
                try:
                    number_stack.append(operations[c](op1, op2))
                except ExponentialError:
                    if is_float(op2):
                        if op2 == math.floor(op2) or op2 == math.ceil(op2):
                            op2 = int(op1)
                            number_stack.append(operations[c](op1, op2))
                        else:
                            calc_flag = 1
                            calc.errorLabel.setText("The exponent must be an integer.")
                            break
                    else:
                        calc_flag = 1
                        calc.errorLabel.setText("The exponent must be an integer.")
                        break
                except RootError:
                    if is_float(op1):
                        if op1 == math.floor(op1) or op1 == math.ceil(op1):
                            op1 = int(op1)
                            number_stack.append(operations[c](op1, op2))
                        else:
                            calc_flag = 1
                            calc.errorLabel.setText("The n-th root must be an integer.")
                            break
                    else:
                        calc_flag = 1
                        calc.errorLabel.setText("The n-th root must be an integer.")
                        break
            else:
                op = number_stack.pop()
                number_stack.append(operations[c](op))
    if calc_flag == 0:
        calc.textfield_as_list.clear()

        temp = str(number_stack.pop())
        calc.textfield.setText(temp)
        calc.textfield_as_list.append(temp)
    calc_flag = 0

def is_float(number):
    """Return if number is float.

    Parameters
    ----------
    number : int, float, string
        Variable containing number to check.

    Returns
    -------
    boolean
        True or False depending on the result.

    """
    try:
        number = float(number)
        return True
    except ValueError:
        return False