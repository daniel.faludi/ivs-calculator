from matlib import add, sub, div, mult, exp, root

with open('/dev/stdin', 'r') as file:
    numbers = file.read() 
    numbers = numbers.splitlines()

numbers = [float(n) for n in numbers]

sum = 0

#Sum of numbers.
for n in numbers:
    sum = add(sum, n)

#Sum / total count.
_x = div(sum, len(numbers))

#Sum of squared numbers
sum = 0
for n in numbers:
    sum = add(sum, exp(n, 2))

#Calculates content of parentheses.
parentheses = sub(sum,mult(len(numbers), exp(_x, 2)))

#1/(N-1)
s = div(1,(len(numbers)-1))

#Multiplies last two calculations.
s = mult(s, parentheses)

#sqrt(s)
s = root(2, s)

print(s)