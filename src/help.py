from PyQt5 import QtWidgets as Qw
from PyQt5 import QtCore as Qc

class helpGUI(Qw.QMainWindow):
    """Class for the user help window."""
    def __init__(self, parent):
        """Parameters
        ----------
        parent : QtWidgets.QMainWindow
            This window's parent window. The help window gets bound to the parent window,
            so that when the parent is closed, the child closes as well."""
        super(helpGUI, self).__init__(parent)
        self.setWindowTitle("Help")
        self.width = 400
        self.height = 500
        self.setFixedSize(self.width, self.height)

        self.centralwidget = Qw.QWidget()
        self.helpText = Qw.QTextBrowser(self.centralwidget)

        self.helpText.setGeometry(Qc.QRect(
                                           5, 5,
                                           self.width-10, self.height-10))
        self.helpText.setReadOnly(True)
        self.helpText.setObjectName("helpText")
        self.setCentralWidget(self.centralwidget)            

        self.setHelpText()

    def setHelpText(self):
        """Sets the user help text in the help window."""
        self.helpText.setHtml(
                "<h1>Calculator help</h1>"
                "<h2>Usage</h2>"
                "<p>"
                "    To use the calculator click on the individual numbers and operators."
                "</p>"
                "<p>"
                "    The calculator prevents malformed inputs such as closing brackets without"
                "    their corresponding opening brackets, stringing together multiple operators"
                "    and beginning a calculation with an operator."
                "</p>"
                "<p>"
                "    The calculator also accepts some keyboard inputs, namely:"
                "    <ul>"
                "        <li>digits <b>0 1 2 3 4 5 6 7 8 9</b></li>"
                "        <li>decimal point <b>.</b></li>"
                "        <li>basic operators <b>+ - * /</b></li>"
                "        <li>brackets <b>()</b></li>"
                "        <li>enter <b>=</b></li>"
                "        <li>backspace/delete <b>Del</b></li>"
                "        <li>escape <b style=\"color: red;\">C</b></li>"
                "    </ul>"
                "    The special functions have no keyboard keys assigned."
                "</p>"
        )