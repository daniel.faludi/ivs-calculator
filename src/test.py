import unittest

import matlib as ml

class TestMatlib(unittest.TestCase):
    def setUp(self):
        self.positiveIntegers = []
        self.negativeIntegers = []
        for i in range(1, 11):
            self.positiveIntegers.append(i)
            self.negativeIntegers.append(i * (-1))
        self.notNumbers = ['a', 'A', str, None]
            
    
    def testAddition(self):
        results = []
        for i, _ in enumerate(self.positiveIntegers):
            results.append(ml.add(self.positiveIntegers[i], self.positiveIntegers[i]))

        results = tuple(results)
        self.assertEqual(results, (2,4,6,8,10,12,14,16,18,20))


    def testSubtraction(self):
        results = []
        for i, _ in enumerate(self.positiveIntegers):
            results.append(ml.sub(self.positiveIntegers[i], self.positiveIntegers[i]))

        results = tuple(results)
        self.assertEqual(results, (0,0,0,0,0,0,0,0,0,0))


    def testMultiplication(self):
        results = []
        for i, _ in enumerate(self.positiveIntegers):
            results.append(ml.mult(self.positiveIntegers[i], self.positiveIntegers[i]))

        results = tuple(results)
        self.assertEqual(results, (1,4,9,16,25,36,49,64,81,100))


    def testDivision(self):
        results = []
        for i, _ in enumerate(self.positiveIntegers):
            results.append(ml.div(self.positiveIntegers[i], self.positiveIntegers[i]))

        results = tuple(results)
        self.assertEqual(results, (1,1,1,1,1,1,1,1,1,1))


    def testFactorial(self):
        results = []
        for i, _ in enumerate(self.positiveIntegers):
            results.append(ml.fact(self.positiveIntegers[i]))

        results = tuple(results)
        self.assertEqual(results, (1,2,6,24,120,720,5040,40320,362880,3628800))


    def testExponential_valid(self):
        valid_exponents = self.positiveIntegers
        for _, v in enumerate(valid_exponents):
            self.assertEqual(ml.exp(v,v), v**v)


    def testExponential_invalid(self):
        # Why assertRaises is within a context manager:
        #    https://ongspxm.github.io/blog/2016/11/assertraises-testing-for-errors-in-unittest/
        
        # Test with invalid base, valid exponent
        invalid_bases = ['a', '12', str, None]
        for _, v in enumerate(invalid_bases):
            with self.assertRaises(ml.ExponentialError): ml.exp(v, 2)

        # Test with valid base, invalid exponent
        invalid_exponents = [-1.5, 0.5, 1.5, 'a', '12', str, None]
        for _, v in enumerate(invalid_exponents):
            with self.assertRaises(ml.ExponentialError): ml.exp(2, v)

        # Test with invalid base, invalid exponent
        for _, v in enumerate(invalid_exponents):
            for _, u in enumerate(invalid_bases):
                with self.assertRaises(ml.ExponentialError): ml.exp(u, v)


    def testRoot_valid(self):
        valid_bases = self.negativeIntegers + [0] + self.positiveIntegers + [-1.5, 2.5]
        for _, v in enumerate(valid_bases):
            self.assertEqual(ml.root(2, v), v**(1/2))

        valid_exponents = self.positiveIntegers
        for _, v in enumerate(valid_exponents):
            self.assertEqual(ml.root(v, 2), 2**(1/v))

    
    def testRoot_invalid(self):
        # Test with invalid base, valid exponent
        invalid_bases = self.notNumbers
        for _, v in enumerate(invalid_bases):
            with self.assertRaises(ml.RootError): ml.root(2, v)

        # Test with valid base, invalid exponent
        invalid_exponents = self.notNumbers + [0] + self.negativeIntegers + [-1.5]
        for _, v in enumerate(invalid_exponents):
            with self.assertRaises(ml.RootError): ml.root(v, 2)

        # Test with invalid base, invalid exponent
        for _, v in enumerate(invalid_exponents):
            for _, u in enumerate(invalid_bases):
                with self.assertRaises(ml.RootError): ml.root(v, u)
        


if __name__ == '__main__':
    unittest.main()