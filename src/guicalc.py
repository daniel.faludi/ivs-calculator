from PyQt5 import QtWidgets as Qw
from PyQt5 import QtGui as Qg
from PyQt5 import QtCore as Qc
import sys

from matlib import solve_equation

from help import helpGUI

class calcGUI(Qw.QMainWindow):

    #Initialization of GUI.
    def __init__(self):
        super(calcGUI, self).__init__()
        self.setFixedSize(700, 800)
        self.installEventFilter(self)
        #Creates main 'screen' of calculator.
        self.errorLabel = Qw.QLabel("", self)
        self.errorLabel.resize(300,20)
        self.errorLabel.move(30, 30)
        self.textfield = Qw.QLineEdit("", self)
        self.textfield.setFocusProxy(self)
        self.textfield.setReadOnly(True)
        self.textfield.resize(650,60)
        font = Qg.QFont("Times", 20)
        self.textfield.setFont(font)
        self.textfield.setAlignment(Qc.Qt.AlignRight | Qc.Qt.AlignVCenter)
        self.textfield.setStyleSheet("background-color: #D4D4D4;")
        self.textfield.move(30, 250)

        self.textfield_as_list = [] #Internal storage of expression.
        self.left_paren_count = 0 #For proper parentheses handling.

        self.init_Buttons()

        self.helpGUI = helpGUI(self)
        self.setupMenuBar()

    def setupMenuBar(self):
        """Sets up the menubar in the UI"""
        helpAction = Qw.QAction("User &help", self, shortcut="F1",
                                triggered=(lambda : self.helpGUI.show())) #Show the help window
        
        menubar = self.menuBar()
        
        helpMenu = menubar.addMenu("Help")
        helpMenu.addAction(helpAction)
        
    def init_Buttons(self):
        """
        Generates all buttons, assigns functions to them and values.

        Parameters
        ----------
        self : calcGUI
            Object of class calcGUI.
        """
        #Numbers
        button0 = Qw.QPushButton('0', self)
        button0.clicked.connect(lambda: self.addCharToLabel('0'))
        button1 = Qw.QPushButton('1', self)
        button1.clicked.connect(lambda: self.addCharToLabel('1'))
        button2 = Qw.QPushButton('2', self)
        button2.clicked.connect(lambda: self.addCharToLabel('2'))
        button3 = Qw.QPushButton('3', self)
        button3.clicked.connect(lambda: self.addCharToLabel('3'))
        button4 = Qw.QPushButton('4', self)
        button4.clicked.connect(lambda: self.addCharToLabel('4'))
        button5 = Qw.QPushButton('5', self)
        button5.clicked.connect(lambda: self.addCharToLabel('5'))
        button6 = Qw.QPushButton('6', self)
        button6.clicked.connect(lambda: self.addCharToLabel('6'))
        button7 = Qw.QPushButton('7', self)
        button7.clicked.connect(lambda: self.addCharToLabel('7'))
        button8 = Qw.QPushButton('8', self)
        button8.clicked.connect(lambda: self.addCharToLabel('8'))
        button9 = Qw.QPushButton('9', self)
        button9.clicked.connect(lambda: self.addCharToLabel('9'))
        
        #Basic operators.
        button_float_point = Qw.QPushButton('.', self)
        button_float_point.clicked.connect(lambda: self.addCharToLabel('.'))

        button_eq_sign = Qw.QPushButton('=', self)
        button_eq_sign.clicked.connect(lambda: solve_equation(self, self.textfield_as_list))

        button_plus_sign = Qw.QPushButton('+', self)
        button_plus_sign.clicked.connect(lambda: self.addCharToLabel('+'))

        button_minus_sign = Qw.QPushButton('-', self)
        button_minus_sign.clicked.connect(lambda: self.addCharToLabel('-'))

        button_mul_sign = Qw.QPushButton('*', self)
        button_mul_sign.clicked.connect(lambda: self.addCharToLabel('*'))

        button_div_sign = Qw.QPushButton('/', self)
        button_div_sign.clicked.connect(lambda: self.addCharToLabel('/'))

        button_left_bracket = Qw.QPushButton('(', self)
        button_left_bracket.clicked.connect(lambda: self.addCharToLabel('('))

        button_right_bracket = Qw.QPushButton(')', self)
        button_right_bracket.clicked.connect(lambda: self.addCharToLabel(')'))

        #Complex operators.
        button_pwr = Qw.QPushButton('x\u207F', self)
        button_pwr.clicked.connect(lambda: self.addCharToLabel('^'))
        
        button_sqrt = Qw.QPushButton('\u207F\u221Ax', self)
        button_sqrt.clicked.connect(lambda: self.addCharToLabel('\u221A'))
        
        button_fact = Qw.QPushButton('n!', self)
        button_fact.clicked.connect(lambda: self.addCharToLabel('!'))

        button_sin = Qw.QPushButton('sin(x)', self)
        button_sin.clicked.connect(lambda: self.addCharToLabel('sin('))

        #Utility buttons.
        button_delete = Qw.QPushButton('Del', self)
        button_delete.clicked.connect(lambda: self.removeChar())

        button_clear = Qw.QPushButton('C', self)
        button_clear.setStyleSheet("color: red")
        button_clear.clicked.connect(lambda: self.clearLabel())

        #Creates list of buttons to iterate through.
        buttons = [button0, button_float_point, button_fact, button_div_sign, button_sin, button_eq_sign, button1, button2, button3, button_mul_sign, button_pwr, button_sqrt, button4, button5, button6, button_minus_sign, button_left_bracket, button_right_bracket, button7, button8, button9, button_plus_sign, button_delete, button_clear]

        #Moves each button to correct position.
        font = Qg.QFont("Times", 14)
        moveX = 30
        moveY = 650
        for i,b in enumerate(buttons):
            b.resize(100,100)
            b.setFocusPolicy(0x1)
            if i % 6 == 0 and i != 0:
                moveX = moveX - 660
                moveY = moveY - 110
            b.move(moveX, moveY)
            b.setFont(font)
            moveX = moveX + 110

    def keyPressEvent(self, event):
        """
        Calls coresponding function on key press.

        Parameters
        ----------
        self : calcGUI
            Object of class calcGUI.
        event : KeyPressEvent
            Holds information about pressed key.
        """
        if event.key() == Qc.Qt.Key_0:
            self.addCharToLabel('0')
        elif event.key() == Qc.Qt.Key_1:
            self.addCharToLabel('1')
        elif event.key() == Qc.Qt.Key_2:
            self.addCharToLabel('2')
        elif event.key() == Qc.Qt.Key_3:
            self.addCharToLabel('3')
        elif event.key() == Qc.Qt.Key_4:
            self.addCharToLabel('4')
        elif event.key() == Qc.Qt.Key_5:
            self.addCharToLabel('5')
        elif event.key() == Qc.Qt.Key_6:
            self.addCharToLabel('6')
        elif event.key() == Qc.Qt.Key_7:
            self.addCharToLabel('7')
        elif event.key() == Qc.Qt.Key_8:
            self.addCharToLabel('8')
        elif event.key() == Qc.Qt.Key_9:
            self.addCharToLabel('9')
        elif event.key() == Qc.Qt.Key_Backspace or event.key() == Qc.Qt.Key_Delete:
            self.removeChar()
        elif event.key() == Qc.Qt.Key_Escape:
            self.clearLabel()
        elif event.key() == Qc.Qt.Key_ParenLeft:
            self.addCharToLabel('(')
        elif event.key() == Qc.Qt.Key_ParenRight:
            self.addCharToLabel(')')
        elif event.key() == Qc.Qt.Key_Plus:
            self.addCharToLabel('+')
        elif event.key() == Qc.Qt.Key_Minus:
            self.addCharToLabel('-')
        elif event.key() == Qc.Qt.Key_Asterisk:
            self.addCharToLabel('*')
        elif event.key() == Qc.Qt.Key_Slash:
            self.addCharToLabel('/')
        elif event.key() == Qc.Qt.Key_Period:
            self.addCharToLabel('.')
        elif event.key() == Qc.Qt.Key_Return or event.key() == Qc.Qt.Key_Enter:
            solve_equation(self, self.textfield_as_list)
        super(type(self), self).keyPressEvent(event)

    def addCharToLabel(self,char):
        """
        Adds character to label and expression list.

        Parameters
        ----------
        self : calcGUI
            Object of class calcGUI.
        char : char
            Character to be added.
        """
        self.errorLabel.setText('')

        text = self.textfield.text()
        
        if char == '.':
            if text:
                if text[-1].isnumeric():
                    text += char
                    self.textfield_as_list[-1] = self.textfield_as_list[-1] + '.'
        elif char.isdigit():
            if text:
                if text[-1] in {'!', ')'}:
                    pass
                else:
                    if text[-1].isnumeric():
                        self.textfield_as_list[-1] = self.textfield_as_list[-1] + char
                    elif text[-1].endswith('.'):
                        self.textfield_as_list[-1] = self.textfield_as_list[-1] + char
                    else:
                        self.textfield_as_list.append(char)
                    text += char
            else:
                text += char   
                self.textfield_as_list.append(char)     
        elif char in {'+', '-', '/', '*', '!', '^', '\u221A'}:
            if text:
                if text[-1] == char:
                    if char == '!' or char == '\u221A':
                        text += char
                        self.textfield_as_list.append(char)
                else:
                    if text[-1] in {'+', '-', '/', '*', '!', '^', '(', '\u221A', '.'}:
                        pass
                    else:
                        text += char
                        self.textfield_as_list.append(char)
        elif char in {'sin(', '('}:
            if text:
                if text[-1] == ')' or text[-1] == '.' or text[-1].isdigit() :
                    pass
                else:
                    if char == '(' or char == 'sin(':
                        self.left_paren_count = self.left_paren_count + 1
                    text += char
                    self.textfield_as_list.append(char)
            else:
                text += char
                self.left_paren_count = self.left_paren_count + 1
                self.textfield_as_list.append(char)
        elif char == ')':
            if text:
                if self.left_paren_count > 0:
                    if text[-1].isdigit() or text[-1] in {')', '!'}:
                        text += char
                        self.left_paren_count = self.left_paren_count - 1
                        self.textfield_as_list.append(')')

        self.textfield.setText(text)

    def removeChar(self):
        """
        Removes last character in label and expression list.

        Parameter
        ---------
        self : calcGUI
            Object of class calcGUI.
        """
        self.errorLabel.setText('')
        text = self.textfield.text()
        text = text[:-1]
        self.textfield.setText(text)
        if self.textfield_as_list:
            temp = self.textfield_as_list.pop()
            if temp == 'sin(' or temp == '(':
                self.left_paren_count = self.left_paren_count - 1
            elif temp == ')':
                self.left_paren_count = self.left_paren_count + 1
            if len(temp) > 1 and temp != 'sin(':
                temp = temp[:-1]
                self.textfield_as_list.append(temp)

    def clearLabel(self):
        """
        Clears whole label and expression list.

        Parameter
        ---------
        self : calcGUI
            Object of class calcGUI.
        """
        self.errorLabel.setText('')
        text = self.textfield.text()
        text = ''
        self.textfield.setText(text)
        self.textfield_as_list.clear()

app = Qw.QApplication(['Calculator'])
gui = calcGUI()
gui.show()
sys.exit(app.exec_())