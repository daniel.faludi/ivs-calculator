﻿# VUT FIT IVS 2019/2020 PROJECT 2 - DOCUMENTATION

 - Patrik Németh (xnemet04)
 - Ján Kučera (xkucer0b)
 - Daniel Faludi (xfalud00)

## License and distribution

This software is distributed under the GNU GPLv1 license.
This software is distributed for the Ubuntu distribution, however can be rebuilt and/or run on other distributions. Details below.
The source can be found on [GitLab](https://gitlab.com/daniel.faludi/ivs-calculator).

## About

Calculator is a software providing graphical user interface built with Python and PyQt for performing simple mathematical operations. This software serves as a school project for IVS course on Brno University of Technology and is distributed under the GNU GPLv1 license.

## Installation

Calculator is built for Linux Debian distributions as a Debian package (.deb) and can be installed as such. You may use package manager of your choice to install this software.

### Installation via terminal

`sudo dpkg -i guicalc.deb`

### Installation via GUI

Double-click the package to run the Ubuntu GUI package installer.

## Uninstallation

To uninstall this software and all of its configurations you may use

`sudo apt-get purge --remove guicalc`.

Or via any graphical package manager of your choosing.

## Manual installation and usage without installation

### Getting the dependedencies

For these use cases you need to download and install some dependencies.
First of all you need Python3 (3.5 or higher) and it's corresponding pip installed. 
Next you need to install the application dependencies. This is done by navigating to the `src/` directory of the project,
opening the terminal in this directory and running the command `make install-deps`. This will install all the dependencies needed for 
launching the app without installation or creating your own installer.

**WARGNING! The makefile is written 'naively', so please don't use** `make` **with the** `-j` **option, as it will likely break the make process.**

#### Making your own installer

After installing the dependencies, navigate to the `src/` directory and run `make deb-pack`. This will create a debian installation package.
From this point you can install the app using the same steps as in section **Installation**.

#### Usage without installation

This software can be used without installation. After **getting the dependencies**, navigate to the `src/` folder in a terminal and run `make run`.
The calculator should pop up.

### Where does the installer place files?

The installer places the application files into three folders:

- `/usr/bin`, where the launch script `guicalc` is placed,
- `/opt`, where the folder `guicalc` with the application is placed,
- `/usr/share/applications`, where the desktop file `guicalc.desktop` is placed.

This app does not come with an icon.

# User Manual

To display user manual in Calculator click on `Help` from top menu and then choose `User help`. Alternatively use F1 shortcut to display user manual.

## Basic usage

- Use graphical numpad in Calculator software or your keyboard numpad to enter simple mathematical expressions
- Press enter or click on `=` button to perform calculation
- You can delete entered characters one by one (by using backspace on your keyboard or `Del` button inside Calculator) or you can delete the whole input using escape key or `C` button

## Usage of more complex mathematical functions

 1. **sin(x)**
 Calculates sine of argument *x*. To use this function press `sin(x)` button inside Calculator and then enter an argument. Argument may be a simple number or a mathematical expression. You can also chain multiple `sin(x)` operations in to one formula.
 2. **x<sup>n</sup>**
Calculates *n-th* power of *x*. Enter the argument *x* first, then click on  <code>x<sup>n</sup></code> button and enter the *n* argument.
3. **<sup>n</sup>√x**
Calculates *n-th* root of *x*. Enter the argument *n* first, then click on <code><sup>n</sup>√x</code> button and enter the *x* argument.
4. **n!**
Calculates factorial of *n*. Enter the argument *n* first, then click on `n!` button.

## What if I enter malformed or non-sensical mathematical expression

The calculator prevents malformed inputs such as closing brackets without their corresponding opening brackets, stringing together multiple operators and beginning a calculation with an operator.
